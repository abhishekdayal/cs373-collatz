#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_calc
# ------------


def collatz_calc(c):
    """
	calculates the collatz cycle length for an integer c
	"""
    cycle = 1
    while c != 1:
        if c % 2 == 0:
            c /= 2
            cycle += 1
        else:
            c = (3 * c) // 2 + 1
            cycle += 2
    return cycle


meta_cache = [
    max([collatz_calc(c) for c in range(x * 1000 + 1, (x + 1) * 1000)])
    for x in range(1000)
]

# ------------
# collatz_eval
# ------------


def collatz_eval(orig_i: int, orig_j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    i, j = orig_i, orig_j
    if orig_i > orig_j:
        i, j = j, i
    mx = 0
    i_c, j_c = (i - 1) // 1000, (j - 1) // 1000
    if (j_c - i_c) <= 1:
        for c in range(i, j + 1):
            mx = max(mx, collatz_calc(c))
    else:
        first = last = 0
        for c in range(i, (1000 * i_c) + 1001):
            first = max(first, collatz_calc(c))
        for c in range(j_c * 1000 + 1, j + 1):
            last = max(last, collatz_calc(c))
        mx = max(first, last, max(meta_cache[i_c + 1 : j_c]))
    return mx


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
